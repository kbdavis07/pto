namespace ACME.DAL.EF
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class TO : DbContext
    {
        // Your context has been configured to use a 'TO' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ACME.DAL.EF.TO' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'TO' 
        // connection string in the application configuration file.
        public TO()
            : base("name=TO")
        {
        }

        public System.Data.Entity.DbSet<ACME.Models.PTOViewModel> PTOViewModels { get; set; }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}