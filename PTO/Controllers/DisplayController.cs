﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ACME.DAL.EF;
using ACME.Models;

namespace PTO.Controllers
{
    public class DisplayController : Controller
    {
        private TO db = new TO();

        // GET: Display
        public ActionResult Index()
        {
            return View(db.PTOViewModels.ToList());
        }

        // GET: Display/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PTOViewModel pTOViewModel = db.PTOViewModels.Find(id);
            if (pTOViewModel == null)
            {
                return HttpNotFound();
            }
            return View(pTOViewModel);
        }

        // GET: Display/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Display/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,EmployeeId,SupervisorName,PTOStartDate,PTOEndDate,PTOType,NumberOfDays")] PTOViewModel pTOViewModel)
        {
            if (ModelState.IsValid)
            {
                db.PTOViewModels.Add(pTOViewModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pTOViewModel);
        }

        // GET: Display/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PTOViewModel pTOViewModel = db.PTOViewModels.Find(id);
            if (pTOViewModel == null)
            {
                return HttpNotFound();
            }
            return View(pTOViewModel);
        }

        // POST: Display/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,EmployeeId,SupervisorName,PTOStartDate,PTOEndDate,PTOType,NumberOfDays")] PTOViewModel pTOViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pTOViewModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pTOViewModel);
        }

        // GET: Display/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PTOViewModel pTOViewModel = db.PTOViewModels.Find(id);
            if (pTOViewModel == null)
            {
                return HttpNotFound();
            }
            return View(pTOViewModel);
        }

        // POST: Display/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PTOViewModel pTOViewModel = db.PTOViewModels.Find(id);
            db.PTOViewModels.Remove(pTOViewModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
