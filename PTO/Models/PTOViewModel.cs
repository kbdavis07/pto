﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace ACME.Models
{
 
  // Drop Down Menu for PTO Type
  public enum PTOType
    {

      [Display(Name = "Vacation")]
      Vacation,

      [Display(Name = "Sick")]
      Sick,

      [Display(Name = "Personal")]
      Personal
   
    }

  public class PTOViewModel             
    {

        [Key]
        public int Id { get; set; }

        [Required]
        [RegularExpression(@"^[a-zA-Z''-'\s]{2,40}$", ErrorMessage = "Only Alphabetical Letters A through Z are allowed.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(@"^[a-zA-Z''-'\s]{2,40}$",ErrorMessage = "Only Alphabetical Letters A through Z are allowed.")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
     
        [Required (ErrorMessage = "You Must Enter In your Employee ID")]    
        [Display(Name = "Employee ID")]
        public string EmployeeId { get; set; }

        [Required]
        [RegularExpression(@"^[a-zA-Z''-'\s]{6,40}$", ErrorMessage = "Must have more than 6 letters and Only Alphabetical Letters A through Z are allowed.")]
        [Display(Name = "Supervisor Full Name")]
        public string SupervisorName { get; set; }

        [Required(ErrorMessage = "Must Enter A Start Date for your Request")]
        [Display(Name = "Start Date")]
        public DateTime PTOStartDate { get; set; }

        [Required(ErrorMessage = "Must Enter an End Date that is after the Start Date.")]
        [Display(Name = "End Date")]
        public DateTime PTOEndDate { get; set; }

        [Required(ErrorMessage = "Must Enter PTO TYPE.")]
        [Display(Name = "Type of PTO")]
        public PTOType? PTOType { get; set; }

        public int NumberOfDays { get; set; } 
       
    }
}