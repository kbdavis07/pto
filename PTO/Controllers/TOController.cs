﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ACME.DAL.EF;
using ACME.Models;
using System.Web.Script.Serialization;
using System.Security.Cryptography;

namespace ACME.Controllers
{
public class TOController : Controller
    {
        private TO db = new TO();

#region Properties

       //Session Property
       public List<PTOViewModel> UserSession
       {
           get
           {
               if (Session["UserFormList"] == null)
                   Session["UserFormList"] = new List<PTOViewModel>();
               return Session["UserFormList"] as List<PTOViewModel>;
           }
           set
           {
               Session["UserFormList"] = value;
           }
       }

#endregion

#region Pages

       // Step 1: Collecting Data From User
        public ActionResult Index()
        {
            ViewBag.Title = "Step:1 Personal Time Off Request";
            return View();
        }
       
       //Step 1: Posting --> to Step 2
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(PTOViewModel model)
        {
            //ToDo: Add Validation logic

            //Adds All Form fields from Step1 to User Session
            Session["Step1"] = model;
           
            // Adds Form data to Session List
            UserSession.Add(model);
            AddCookie();
            return RedirectToAction("Details", "TO");       
        }

        //Step 2 Getting Data From User

        public ActionResult Details()  
        {

            ViewBag.Title = "Step 2: Pick Dates and Type";
            return View();
        }

        //Step 2 Posting Going to Final Step 3

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details(PTOViewModel model)
        {

            // Validation logic
            if (model.PTOEndDate <= model.PTOStartDate)
            {
                ModelState.AddModelError("PTOEndDate", "End Date must be after the Start Date.");  //Todo: Show error at Confirmation Step
                model.PTOEndDate = model.PTOStartDate.AddDays(2);    //Adds 1 Day to End date to make sure End date is after Start date
                return View("Details");
            }

            TimeSpan PTODays = model.PTOEndDate - model.PTOStartDate;
            model.NumberOfDays = (int)PTODays.TotalDays;        //end date - start date + 1

            //Adds All Form fields to User Session from Step2
            Session["Step2"] = model;

           
            // Adds Form data to Session List
            UserSession.Add(model);
            AddCookie();
           
            return RedirectToAction("success");

        }
        //Final Step 3
        public ActionResult success()
        {
            ViewBag.Title = "Step 3: Personal Time Off (PTO) Request Confirmation";
            return View();
        }


#endregion

#region Methods

        public void AddCookie()
        {
            if (!this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("FormData"))
            {
                //Adding Cookie to User Device, Just in case Session times out before completing form
                string myObjectJson = new JavaScriptSerializer().Serialize(UserSession);

                //MD5(myObjectJson);

                var UserForm = new HttpCookie("FormData", myObjectJson) { Expires = DateTime.Now.AddDays(1) };
                this.ControllerContext.HttpContext.Response.Cookies.Add(UserForm);
            }

            else if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("FormData"))
            {
                string myObjectJson = new JavaScriptSerializer().Serialize(UserSession);
                this.ControllerContext.HttpContext.Request.Cookies["FormData"].Value = myObjectJson;

            }

        }

#endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
